<?php

/**
* 
*/
interface Modelo{

 	public function newId($fichero);

	public function createLocalizacion($localizacion);	
	public function createUsuario($usuario);

	public function readLocalizaciones();
	public function readUsuarios();

	public function updateLocalizacion($localizacion);	
	public function updateUsuario($usuario);

	public function deleteLocalizacion($localizacion);
	public function deleteUsuario($usuario);
}

?>