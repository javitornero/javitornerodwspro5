<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');

	include_once("Config.php");
	
	include_once("Modelo.php");

	function obtenerModelo() {
	    $tipo = Config::$modelo;
	    switch ($tipo) {
	    	case 'fichero':
				include_once("ModeloFichero.php");
	    		$modelo = new ModeloFichero();
	    		break;
	    }
	    return $modelo;
	}


    include_once("./model/Localizacion.php");
	include_once("./model/Usuario.php");
    
    include_once("./view/utils_view.php");    
?>