	<?php include_once('controller/utils_control.php'); ?>
<html lang="es">
<?php include('view/head.php'); ?>
<body>
	<div id="divCabecera">
			<?php
				include('view/divLogo.php');
				include('view/menu.php');
			?>
	</div>
	<div id="contenedor">
		<?php

		$modelo=obtenerModelo(); // al ser redirigido hasta este nuevo scrtipt desde los action de los form hay que declarar una nueva variable modelo

		// Función que limpia la entrada de datos en formularios
		function recoge($campo) { 
			if (isset($_REQUEST[$campo])) {
				$valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
			} else {
				$valor = "";
			};
			return $valor;
		}

		//**********************************
		// Funciones para el CREATE

		if (isset($_REQUEST["sbCreateLoc"])) {
			$id = recoge("id");
			$nom = recoge("nom");
			if ($id!="" && $nom!="") {
				$localizacion = new Localizacion($id, $nom);
				echo $modelo->createLocalizacion($localizacion); // Añadimos los datos del nuevo objeto
			} else {
				echo "Operación no realizada, se han encontrado campos vacíos.";
			}
		}

		if (isset($_REQUEST["sbCreateU"])) {
			$id = recoge("id");
			$nom = recoge("nom");
			$id_loc = recoge("id_loc");
			if ($id!="" && $nom!="" && $id_loc!="") {
				$localizacion = new Localizacion($id_loc, "");
				$usuario = new Usuario($id, $nom, $localizacion);
				echo $modelo->createUsuario($usuario); // Añadimos los datos del nuevo objeto
			} else {
				echo "Operación no realizada, se han encontrado campos vacíos.";
			}
		}

		//**********************************
		// Funciones para el READ

		// Función que pinta una tabla con una vista de los objetos Localizaciones
		function pintaTablaLoc($modelo){
			$arrayObjs=$modelo->readLocalizaciones();
			$row=count($arrayObjs);
			echo "<table>";
			echo "<tr class='cabTabla'><td class='colTabla'>Id</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Nombre</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Usuarios</td></tr>";
			for($i=0;$i<$row;$i++){
				echo "<tr class='filaTabla'>";
				echo "<td class='colTabla'>".$arrayObjs[$i]->getId()."</td>";
				echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;".$arrayObjs[$i]->getNom()."</td>";
				echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$modelo->countUsuariosByLoc($arrayObjs[$i])."</td>";
				echo "</tr>";
			}
			echo "</table>";
		}
		if (isset($_REQUEST["sbReadLoc"])) { ?>
			<div class="divReadRes" id="divReadLoc">
				<h1>Localizaciones</h1>
				<div class="panelScroll">
					<?php pintaTablaLoc($modelo); ?>
				</div>
			</div>
		<?php }

		// Función que pinta una tabla con una vista de los objetos Usuarios
		function pintaTablaU($modelo){
			$arrayObjs=$modelo->readUsuarios();
			$row=count($arrayObjs);
			echo "<table>";
			echo "<tr class='cabTabla'><td class='colTabla'>Id</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Nombre</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Localización</td></tr>";
			for($i=0;$i<$row;$i++){
				echo "<tr class='filaTabla'>";
				echo "<td class='colTabla'>".$arrayObjs[$i]->getId()."</td>";
				echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;".$arrayObjs[$i]->getNom()."</td>";
				echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;".$arrayObjs[$i]->getLocalizacion()->getNom()."</td>";
				echo "</tr>";
			}
			echo "</table>";
		}

		if (isset($_REQUEST["sbReadU"])) { ?>
			<div class="divReadRes" id="divReadU">
				<h1>Usuarios</h1>
				<div class="panelScroll">
					<?php pintaTablaU($modelo); ?>
				</div>
			</div>
		<?php }

		//**********************************
		// Funciones para el UPDATE

		if (isset($_REQUEST["sbUpdateLoc"])) {
			$id = recoge("id");
			$nom = recoge("nom");
			if ($id!="" && $nom!="") {
				$localizacion = new Localizacion($id, $nom);
				echo $modelo->updateLocalizacion($localizacion);
			} else {				
				echo "Operación no realizada, se han encontrado campos vacíos.";
			}
		}

		if (isset($_REQUEST["sbUpdateU"])) {
			$id = recoge("id");
			$nom = recoge("nom");	
			$id_loc = recoge("id_loc");
			if ($id!="" && $nom!="" && $id_loc!="") {
				$localizacion = new Localizacion($id_loc, "");
				$usuario = new Usuario($id, $nom, $localizacion);
				echo $modelo->updateUsuario($usuario);
			} else {				
				echo "Operación no realizada, se han encontrado campos vacíos.";
			}
		}

		//**********************************
		// Funciones para el DELETE

		//////////////////////////////////////////////////////////////////////////

		if (isset($_REQUEST["sbDeleteLoc"])) {
			$id = recoge("id");
			if ($id!="") {
				$localizacion = new Localizacion($id, "");
				echo $modelo->deleteLocalizacion($localizacion);
			} else {				
				echo "Operación no realizada, se han encontrado campos vacíos.";
			}
		}

		if (isset($_REQUEST["sbDeleteU"])) {
			$id = recoge("id");
			if ($id!="") {
				$localizacion = new Localizacion("", "");
				$usuario = new Usuario($id, "", $localizacion);
				echo $modelo->deleteUsuario($usuario);
			} else {				
				echo "Operación no realizada, se han encontrado campos vacíos.";
			}
		} 

		?>
	</div>
	<footer>
		<?php include('view/pie.php'); ?>		
	</footer>
</body>
</html>