<?php
/**
* 
*/
class Localizacion {
	//Propiedades
	private $id;
	private $nom;
	//Constructor
	public function __construct($id, $nom) {
		$this->id = $id;
		$this->nom = $nom;
	}
	//Metodos
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
	}
	public function getNom() {
		return $this->nom;
	}
	public function setNom($nom) {
		return $this->nom = $nom;
	}
}
?>