<?php
/**
* 
*/
class Usuario {
	//Propiedades
	private $id;
	private $nom;
	private $localizacion;
	//Constructor
	public function __construct($id, $nom, $localizacion) {
		$this->id = $id;
		$this->nom = $nom;
		$this->localizacion = $localizacion;
	}
	//Metodos
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
	}
	public function getNom() {
		return $this->nom;
	}
	public function setNom($nom) {
		return $this->nom = $nom;
	}
	public function getLocalizacion() {
		return $this->localizacion;
	}
	public function setLocalizacion($localizacion) {
		return $this->localizacion = $localizacion;
	}
}
?>