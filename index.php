<?php
	include_once('controller/utils_control.php');
	$modelo=obtenerModelo();
?>
<html lang="es">
<?php include('view/head.php'); ?>
<body>
	<div id="divCabecera">
		<?php
			include('view/divLogo.php');
			include('view/menu.php');
		?>
	</div>
	<div id="contenedor">
		<?php
		if (isset($_GET["show"])) {
			switch ($_GET["show"]) {
				case 'addLoc':
				include('view/createLoc.php');
				break;
				case 'showLoc':
				include('view/readLoc.php');
				break;
				case 'upDateLoc':
				include('view/updateLoc.php');
				break;
				case 'delLoc':
				include('view/deleteLoc.php');
				break;
				case 'addU':
				include('view/createU.php');
				break;
				case 'showU':
				include('view/readU.php');
				break;
				case 'upDateU':
				include('view/updateU.php');
				break;
				case 'delU':
				include('view/deleteU.php');
				break;
			}
		}
		?>
	</div>
	<footer>
		<?php include('view/pie.php'); ?>		
	</footer>
</body>
</html>