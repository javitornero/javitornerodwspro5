<div class="contenedorForm">
	<span class="cierraForm"><a class="aCierraForm" href="index.php">&nbsp;x&nbsp;</a></span>
	<div class="divCRUD" id="divUpDateLoc">
		<h1>Actualizar localización</h1>
		<form method="POST" action="acciones.php" >
			<table>
				<tr>
					<td class="tdCRUD">Localización a actualizar: </td>
					<td class="tdCRUD">
						<select name="id" required >
							<?php
								pintaOptions($modelo->readLocalizaciones());
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tdCRUD">Nuevo nombre localización: </td>
					<td class="tdCRUD"><input class="textCRUD" type="text" name="nom" required /></td>
				</tr>
				<tr>
					<td class="tdCRUD"><input type="submit" name="sbUpdateLoc" value="Actualizar"></td>
					<td class="tdCRUD"><input type="reset" name="Borrar"></td>
				</tr>
			</table>
		</form>
	</div>
</div>