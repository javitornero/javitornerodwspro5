<div class="contenedorForm">
	<span class="cierraForm"><a class="aCierraForm" href="index.php">&nbsp;x&nbsp;</a></span>
	<div class="divCRUD" id="divUpDateU">
		<h1>Actualizar usuario</h1>
		<form method="POST" action="acciones.php" >
			<table>
				<tr>
					<td class="tdCRUD">Usuario a actualizar: </td>
					<td class="tdCRUD">
						<select name="id" required >
							<?php
								pintaOptions($modelo->readUsuarios());
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tdCRUD">Actualice nombre: </td>
					<td class="tdCRUD"><input class="textCRUD" type="text" name="nom" required /></td>
				</tr>
				<tr>
					<td class="tdCRUD">Actualice localización: </td>
					<td class="tdCRUD">
						<select name="id_loc" required >
							<?php
								pintaOptions($modelo->readLocalizaciones());
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tdCRUD"><input type="submit" name="sbUpdateU" value="Actualizar"></td>
					<td class="tdCRUD"><input type="reset" name="Borrar"></td>
				</tr>
			</table>
		</form>
	</div>
</div>