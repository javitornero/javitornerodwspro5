<?php

// Función que pinta el nombre de un objeto en los option de un select leyendo de un array de objetos
function pintaOptions($arrayObjs){
	if ($arrayObjs) {
		echo "<option selected disabled>Elige opción</option>";
	} else{
		echo "<option selected disabled>No hay localizaciones creadas.</option>";	
	}
	for($i=0;$i<count($arrayObjs);$i++){
		echo "<option value=".$arrayObjs[$i]->getId().">id: ".$arrayObjs[$i]->getId()." - ".$arrayObjs[$i]->getNom()."</option>";
	}
}


?>