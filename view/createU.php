<div class="contenedorForm">
	<span class="cierraForm"><a class="aCierraForm" href="index.php">&nbsp;x&nbsp;</a></span>
	<div class="divCRUD" id="divCreateU">
		<h1>Nuevo usuario</h1>
		<form method="POST" action="acciones.php" >
			<input type="hidden" name="id" value="<?php echo $modelo->newId('usuarios'); ?>"/>
			<table>
				<tr>
					<td class="tdCRUD">Nombre nuevo usuario: </td>
					<td class="tdCRUD"><input class="textCRUD" type="text" name="nom" required /></td>
				</tr>
				<tr>
					<td class="tdCRUD">Localización del usuario: </td>
					<td class="tdCRUD">
						<select name="id_loc" required >
							<?php
								pintaOptions($modelo->readLocalizaciones());
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tdCRUD"><input type="submit" name="sbCreateU" value="Crear"></td>
					<td class="tdCRUD"><input type="reset" name="Borrar"></td>
				</tr>
			</table>
		</form>
	</div>
</div>