<div class="contenedorForm">
	<span class="cierraForm"><a class="aCierraForm" href="index.php">&nbsp;x&nbsp;</a></span>
	<div class="divCRUD" id="divDelLoc">
		<h1>Eliminar localización</h1>
		<form method="POST" action="acciones.php" >
			<table>
				<tr>
					<td class="tdCRUD">Seleccione localización a eliminar: </td>
					<td class="tdCRUD">
						<select name="id" required >
							<?php pintaOptions($modelo->readLocalizaciones()); ?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tdCRUD" colspan="2">
						<p class="aviso">Aviso: Sus usuarios también serán borrados.</p>
					</td>
				</tr>
				<tr>
					<td class="tdCRUD"><input type="submit" name="sbDeleteLoc" value="Eliminar"></td>
					<td class="tdCRUD"><input type="reset" name ="Borrar"></td>
				</tr>
			</table>
		</form>
	</div>
</div>